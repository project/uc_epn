
This is a payment gateway module for eProcessingNetwork's Transparent
Database Engine (TDBE) API. It has experimental support for the development
uc_recurring-6.x-2.x API for recurring payments, using the ability to 
store credit card details at eProcessingNetwork and then make charges with
the returned transaction ID and no credit card #.

It is recommended to use the latest version of Ubercart 6.x with this
module. 

INSTALLATION AND SETUP

a) Install and enable the module in the normal way for Drupal.

b) Visit your Ubercart Store Administration page, Configuration
section, and enable the gateway under the Payment Gateways.
(admin/store/settings/payment/edit/gateways)

c) On this page, provide the following settings: 
   - Your epn Account Number
   - A RestrictKey generated in the EPN processing control page
   - Authentication type 

Note that the default account number and restrict key are for a test
account at EPN. You can log into the test merchant account at 
https://www.eprocessingnetwork.com, with the account 080880 and password
080880pw - any transactions you post should be listed in the current batch.

Supports recurring payments with uc_recurring 2.x series (currently in Alpha).
